import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { JobDetailsComponent } from './job-details/job-details.component';
import { ConditionsComponent } from './conditions/conditions.component';
import { PrivacyComponent } from './privacy/privacy.component';

const routes: Routes = [
  { path: '', redirectTo: '?', pathMatch: 'full' },
  { path: '', component: HomeComponent},
  { path: 'home', component: HomeComponent},
  { path: 'JobDetails', component: JobDetailsComponent},
  { path: 'Conditions', component: ConditionsComponent},
  { path: 'Privacy', component: PrivacyComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
